###Introduction
**Simple Linq** is a simple JavaScript library that I have created based on a feature of C# called **LINQ**.

###Download
You can pull it from my repository on [BitBucket](https://bitbucket.org/no1_melman/simplelinq/).

It is currently a beta.

###Aim
There are many JavaScript libraries out there which will happily give you all the **LINQ** methods and the ability to write Lambda expressions (in the form of a string) to query an object.

I want something much, much simpler. Something that people could happily tag onto as well, right in their scripts.

This means **few methods -> less footprint -> small documentation -> quicker understanding**.

###Current Features
Some current features

1. 3 Linq Methods
2. Prototype Pattern, Fluent Interface Pattern
####Tutorial
Common usages:
#####Constructing
```javascript
var linqRes = new Linq([{ name: "Jeff", age: 20 },{ name: "Micheal", age: 25 }]);
```
#####Querying
######Where
So you can query two ways, either by using an object or by using a function. If you are using the function the **item** is passed as the parameter. You should return true if you want to keep the item in the list or return false to remove it.

The **function** method allows you to do more complicated filters than the **object** method. The **object** method allows you to quickly filter in a readable way.

This function returns the *Linq* object, it allows chaining.
```javascript
// By Object
linqRes.where({ age: 25 }); // result: { name: "Micheal", age: 25 }

// By Function
linqRes.where(function (e) {
	return e.age === 25;
}); // result: { name: "Micheal", age: 25 }
```
######Count
So this is quite simply a wrap around the length, however you can implement a filter as well. The filter is based on the **where** method so operates in the same way.

This method is a terminal method, the result is a *Number*.

```javascript
linqRes.count(); // result: 2

// By Object
linqRes.count({ age : 25 }); // result: 1

// By Function
linqRes.count(function (e) {
	return e.age === 25;
}); // result: 1
```
######ForEach
This is a simple wrapper around the for loop.

This returns a *Linq* object.	
```javascript
linqRes.forEach(function (e) {
	// do work on each item here
    
    return e; // return the modified element
});
```
######Value
This function simply returns back the original array.

This is a terminal functions and returns **Array**.
```javascript
linqRes.value(); // [{ name: "Jeff", age: 20 },{ name: "Micheal", age: 25 }]
```

#####Usage
This is a very construde example, but shows you from start to finish the usage.

```javascript

function processDbItems(items) {
	// items is an array of objects
    
    // create new linq object
    var linqObj = new Linq(items);
    
    // filter out everyone who earns more than $50,000
    var filteredRes = linqObj.where(function (e) {
    	return e.pay < 50000;
    });
    
    // count the filtered results
    var filteredCount = filteredRes.count();
    // count the unfiltered results
    var unFilteredCount = linqObj.count();
    
    var difference = unFilteredCount - filteredCount;
    
    // bump up everyones salary who is under 50000 by 20%
    var salaryRises = filteredRes.forEach(function (e) {
    	e.pay = e.pay * 1.2;
    });
    
    // now from this list filter everyone who is under 30000 and raise them up to 30000
    // and return the count of these people
    var salariesUnderThirtyK = salaryRises.where(function (e) {
          return e.pay < 30000;
      }).forEach(function (e) {
          e.pay = 30000;
          
          return e;
      }).count();
      
    reportFigures(difference, salariesUnderThirtyK);
}

```
###Current Architecture
The current architecture follows the **self** paradigm:

```javascript
var Linq = function () {
	var self = this;
    
    self.where = function () { return self; }; // public method
    
    function doWhere() {
    
    } // private method
}
```

Utility Methods

* isFunction
	* Returns true if a function, false if an object and null if neither
* testIsFunction
	* Fires a callback depending on whether a function was given or an object
* LinqException
	* This is an internal exception class that should be thrown when Linq has an issue 

#####isFunction
Determines whether an object passed through is a *function* or *object* or neither.
######Syntax
```javascript
function isFunction(obj) { }
```
Parameters:
<table>
	<tr>
    	<td><i>obj</i></td>
        <td></td>
    </tr>
    <tr>
    	<td></td>
        <td>Any object that needs to be determine</td>
    </tr>
</table>

Return Value:
Boolean
**true** if is a *function*; **false** if is an object; **null** if neither.

#####testIsFunction
The idea of this function is to make it really simple when working with either **functions** or **objects**.
######Syntax
Executes a callback that applies logic base on whether the obj is a **function** or an **object**.
```javascript
function testIsFunc(obj, func, callback) { }
```
Parameters:
<table>
	<tr>
    	<td><i>obj</i></td>
        <td></td>
    </tr>
    <tr>
    	<td></td>
        <td>Any object that needs to be determine</td>
    </tr>
	<tr>
    	<td><i>func</i></td>
        <td></td>
    </tr>
    <tr>
    	<td></td>
        <td>The name of the function that is calling this method</td>
    </tr>
	<tr>
    	<td><i>callback</i></td>
        <td></td>
    </tr>
    <tr>
    	<td></td>
        <td>A function that is passed the outcome of determining the object type
        	<br />
            <b>Syntax</b>
            <br />
            <code style="line-height:40px">function (result) {}</code>
            <br />
            <i>Parameters</i>
            <br />
            <table>
              <tr>
                  <td><i>result</i></td>
                  <td></td>
              </tr>
              <tr>
                  <td></td>
                  <td>Boolean, has the result of the determination of the passed <i>obj</i></td>
              </tr>
            </table>
        </td>
    </tr>
</table>
######Example

```javascript

function testIsFunc(obj, func, callback) {
  var isFunc = isFunction(obj);

  if (!isFunc) {
  	return callback(!isFunc);
  }else if (isFunc) {
  	return callback(isFunc);
  } else {
 	 throw new LinqException("Parameter not an object or a function", func);
  }
}

function where(obj) {
	testIsFunc(obj, "where", function (result) {
    	if (result) // is a function
        	executeWhereWithFunction(obj);
        else // is an object
        	executeWhereWithObject(obj);
    });
}

```

#####LinqException
This is a standard framework exception that should be thrown from anywhere that doesn't get what it is expecting, or encounters and problem when executing what has been passed. Anything from validation up to unexpected objects being passed.
######Syntax
```javascript
function LinqException(message, functionName) { }
```

<table>
	<tr>
    	<td><i>message</i></td>
        <td></td>
    </tr>
    <tr>
    	<td></td>
        <td>The exception message</td>
    </tr>
    <tr>
    	<td><i>functionName</i></td>
        <td></td>
    </tr>
    <tr>
    	<td></td>
        <td>The function name that threw the error</td>
    </tr>
</table>

######Examples
```javascript

throw new LinqException("Linq encountered an error", "executeException");

```

---
###Upcoming Features
There are a couple more features I would like to add.

* New Methods:
    * Join
    * Any
    * Select
* Complex object query syntax

#####New Methods
So these are pretty good methods to add, and should keep my plate full for a bit. They are really useful for reducing and running logic from. I will *prioritise* **Select** first, then **Any** and finally **Join**.

#####Complex Object Query Syntax
This is something I have seen in **MongoDb** and I like it. So far the **object** method is a simple equals. I would like to use more operators like this:

```javascript
linqRes.where({ age: { isEq: 20 }}); // is equal to
linqRes.where({ age: { notEq: 20 }}); // not equal to
linqRes.where({ age: { isGt: 20 }}); // is greater than
linqRes.where({ age: { isLt: 20 }}); // is less than

linqRes.where({ age: { isGt: 20, isLt 40 }}); // greater than 20 and less than 40

linqRes.where({ name: { contains: "Jo" }}); // name that contains "Jo"
```
This is a very neat way of producing queries, and is very semantic. The idea is that you can read like this:
```
where - age - is greater than : 20
where - age - not equal to : 20
```

######Multiple Field Filter
This is already implemented, so you can do something like this at the moment:
```javascript
linqRes.where({ age: 20, pay: 30000 });
```

The filter will try and apply all these filter arguments to a single item, and reads like this:

```
where - age - is eqaul to : 20 AND pay - is equal 30000
```

### Features ###
* Using complex object queries

```
#!javascript

linqRes.where({ age: { isEq: 20 }); // equal to
linqRes.where({ age: { isGt: 20 }); // greater than
linqRes.where({ age: { isLt: 20 }); // less than

```