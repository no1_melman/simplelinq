(function (context) {
	// ---------------------------------------------- 
	// ------------ Private Properties   ------------
	// ----------------------------------------------
	
	var Types = {
		Function: typeof function () {},
		Object: typeof {},
		String: typeof "",
		Number: typeof 1,
		Boolean: typeof true,
		Undefined: typeof undefined
	}
	console.log(Types);
	
	var ComparisonOperators = {
		// numeric operators
		lt: function (a,b) { return a < b; },
		gt: function (a,b) { return a > b; },
		eq: function (a,b) { return a===b; },
		not: function (a,b) { return a!==b; },
		ltEq: function (a,b) { return a <== b; },
		gtEq: function (a,b) { return a >== b; },
		
		// string operators
		contains: function (a,b) { return a.indexOf(b) > -1; }
	};
	
	// ---------------------------------------------- 
	// ------------ Linq Object          ------------
	// ----------------------------------------------
	
	var Linq = function (arrayToLinq) {
		this.value = function () { return arrayToLinq; };
	};
	
	// ---------------------------------------------- 
	// ------------ Public Linq Methods  ------------
	// ----------------------------------------------
	
	// simple object cloning.
	Linq.prototype.clone = function(objectToClone) {
		if (objectToClone instanceof Array)
		{
			var clone = [];
			for (var i = 0; i < objectToClone.length; i++)
				clone[i] = this.clone(objectToClone[i]);

			return clone;
		} 
		else if (typeof objectToClone === 'object')
		{
			var clone = {};
			for (var prop in objectToClone)
				if (objectToClone.hasOwnProperty(prop))
					clone[prop] = this.clone(objectToClone[prop]);

			return clone;
		}
		else
			return objectToClone;
	};
	
	// simple for each loop
	// requires the user to return the new value;
	Linq.prototype.forEach = function (modifier) {
		var modifierArray = this.clone(this.value());
		
		if (typeof modifier === Types.Object) {
			throw new LinqException("Expected Function but got Object", "foreach");
		}
		
		for (var i = 0; i < modifierArray.length; i++) {
			modifierArray[i] = modifier(modifierArray[i]);
		}
		
		return new Linq(modifierArray);
	};
	
	// where function
	// if using function, return true or false
	Linq.prototype.where = function (obj) {
		var internalArray = this.value();
	
		var res = testIsFunc(obj, "count", function(ans) {
			if (ans)
				return whereWithFunction(obj, internalArray);
			else if (!ans)
				return whereWithObj(obj, internalArray);
		});
		
		return new Linq(res);
	};
	
	Linq.prototype.count = function (obj) {
		var arrCount = this.clone(this.value());
		
		if (typeof obj !== Types.Undefined) {
			var res = testIsFunc(obj, "count", function(ans) {
				if (ans)
					return whereWithFunction(obj, arrCount);
				else if (!ans)
					return whereWithObj(obj, arrCount);
			});
					
			arrCount = res;
		}
		
		return arrCount.length;
	};
	
	Linq.prototype.any = function (obj) {
		var arr = this.clone(this.value());
		
		if (typeof obj !== Types.Undefined) {
			var res = testIsFunc(obj, "any", function (ans) {
				if (ans)
					return whereWithFunction(obj, arr);
				else if (!ans)
					return whereWithObj(obj, arr);
			});
			
			arr = res;
		}
		
		return arr.length !== 0;
	}
	
	// ---------------------------------------------- 
	// ------------ Private Linq Methods ------------
	// ----------------------------------------------
	
	// Linq Exception Object
	function LinqException(message, functionName) {
		this.message = message;
		this.functionName = functionName;
		this.name = "LinqException";
	}
	
	// Testing a object and calling a callback
	function testIsFunc(obj, func, callback) {
		var isFunc = isFunction(obj);
			
		if (!isFunc) {
			return callback(false);
		}else if (isFunc) {
			return callback(true);
		} else {
			throw new LinqException("Parameter not an object or a function", func);
		}
	}
	
	// Testing an object
	function isFunction(obj){
		return typeof obj === Types.Object ? false : typeof obj === Types.Function ? true : null;
	}
	
	// Returns an array
	function whereWithFunction(func, arr) {
		var newArr = [];
		var internalArray = arr;
		for (var i = 0; i < internalArray.length; i++) {
			var e = internalArray[i];
			var keep = func(e);
			
			if (keep) {
				newArr.push(e);
			}
		}
		
		return newArr;
	}
	
	function whereWithObj(obj, arr) {
		var newArr = [];
		var internalArray = arr;
		for (var i = 0; i < internalArray.length; i++) {
			var keep = false;
			
			for(var key in obj) {
				keep = internalArray[i][key] == obj[key];
			}
			
			if (keep) {
				newArr.push(internalArray[i]);
			}
		}

		return newArr;
	}
	
	// Usages: 	{ name: "callum" }
	//			{ name: { contains: "callum" } }
	//			{ age: 2 }
	//			{ age: { eq: 2 } }
	//			{ isAlive: false }
	//			{ age: { eq: false } }
	// outer object is the PropertySelectorObject
	// the inner object is the ComparisonSelectorObject
	function deterministicObjExecution(obj) {
		// we need to find out what data the key holds
		
		// iterate through the properties in the obj
		// these are properties that correspond to the source obj
		for (var prop in obj) {
			// switch the type of
			switch(typeof obj[key])
			{
				case typeof Types.String:
					// so here we are dealing with a string
					
					break;
				case typeof Types.Number:
					// so here we are dealing with a number
					
					break;
				case typeof Types.Object:
					// so here we are dealing with an object
				
					break;
				case typeof Types.Undefined:
					// here the type is undefined, probs throw an error
				
					break;
			}
		}
		
	}
	
	function doComparisons(propertySelectorObjects, sourceObject) {
		for (var prop in propertySelectorObjects) {
			// prop is the property on the source object we wish to evaluate
			
			// get the source value which we are comparing with
			var sourceObjValue = sourceObject[prop];
			
			// the comparison selector object is the value of the propertySelectorObjects keys
			var comparisonSelectorObject = propertySelectorObjects[prop];
			
			for (var comparisonOperator in comparisonSelectorObject) {
				// make sure the comparison operator exists in our predefined list
				if (typeof ComparisonOperators[comparisonOperator] !== Types.Undefined) {
					// get the value the user wants to compare against
					var comparingObject = comparisonSelectorObject[comparisonOperator];
					// get the function that we need to run in order to compare the values
					// defined by the user
					var comparerFunction = ComparisonOperators[comparisonOperator];
					// execute the compare and get the result
					var result = executeCompare(sourceObjValue, comparingObject, comparerFunction);
					
					return false;
				}
			}
		}
		
		return true;
	}
	
	function executeCompare(source, destination, comparer) {
		return comparer(source, destination);
	}

	// Opening Linq up to the outside world.
	context.Linq = Linq;
	
})(this);